package com.BathUni.EmotivRcCar.Controller;

import com.BathUni.EmotivRcCar.Services.ICameraService;
import com.BathUni.EmotivRcCar.Services.IMotorService;
import com.BathUni.EmotivRcCar.Services.ISteeringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandController {

    private ICameraService cameraService;
    private IMotorService motorService;
    private ISteeringService steeringService;

    private boolean left = false;
    private boolean right = false;
    private boolean neutral = true;

    private boolean foward = false;
    private boolean backward = false;
    private boolean fbNeutral = true;

    @Autowired
    public CommandController(ICameraService cameraService, IMotorService motorService, ISteeringService steeringService) {
        this.cameraService = cameraService;
        this.motorService = motorService;
        this.steeringService = steeringService;
    }

    public void fowardStart() {
        if (fbNeutral) {
            motorService.foward();
            foward = true;
            fbNeutral = false;
        }else if (backward){
            motorService.stop();
            backward = false;
            fbNeutral = true;
        }
    }

    public void backwardStart() {
        if (fbNeutral) {
            motorService.backward();
            backward = true;
            fbNeutral = false;
        }else if (foward){
            motorService.stop();
            foward = false;
            fbNeutral = true;
        }
    }

    public void turnLeft(){
        if (neutral) {
            steeringService.turnLeft();
            left = true;
            neutral = false;
        }else if (right){
            steeringService.home();
            right = false;
            neutral = true;
        }
    }

    public void turnRight(){
        if (neutral) {
            steeringService.turnRight();
            right = true;
            neutral = false;
        }else if (left){
            steeringService.home();
            left = false;
            neutral = true;
        }
    }

    public void resetSteering(){
        steeringService.home();
        right = false;
        left = false;
        neutral = true;
    }

    public void fowardStop() {
        motorService.stop();
        foward = false;
        backward = false;
        fbNeutral = true;
    }

    public void cameraIncX() {
        cameraService.moveIncreaseX();
    }

    public void cameraDecX() {
        cameraService.moveDecreaseX();
    }

    public void cameraIncY() {
        cameraService.moveIncreaseY();
    }

    public void cameraDecY() {
        cameraService.moveDecreaseY();
    }

}
