package com.BathUni.EmotivRcCar;

import com.BathUni.EmotivRcCar.Controller.CommandController;
import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.net.SocketException;
import java.util.List;

@Configuration
@ComponentScan
public class Main {

    // Keep Alive Boolean
    static boolean keepAlive = true;

    public static void main(String[] args) {

        // Get App Context
        ApplicationContext context =
                new AnnotationConfigApplicationContext(Main.class);

        // Create a command Controller
        final CommandController commandController = context.getBean(CommandController.class);

        // OSC Port number
        int OSCportIn = 7400;

        // Directions
        final int FOWARD = 0;
        final int BACKWARD = 1;
        final int LEFT = 2;
        final int RIGHT = 3;
        final int STOP = 4;

        // Commands
        final int END = 5;

        // Camera
        final int UP = 6;
        final int DOWN = 7;
        final int PANLEFT = 8;
        final int PANRIGHT = 9;

        OSCListener directionListener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage directionMessage) {
                // Received command in arguments for direction e.g. left etc..
                List<Object> arguments = directionMessage.getArguments();
                int direction = (Integer) arguments.get(0);

                switch (direction) {
                    case FOWARD:
                        commandController.fowardStart();
                        break;
                    case BACKWARD:
                        commandController.backwardStart();
                        break;
                    case LEFT:
                        commandController.turnLeft();
                        break;
                    case RIGHT:
                        commandController.turnRight();
                        break;
                    case STOP:
                        commandController.fowardStop();
                        commandController.resetSteering();
                }
            }
        };

        OSCListener cameraListener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage cameraMessage) {
                // Received command in arguments for camera e.g. up etc...
                List<Object> arguments = cameraMessage.getArguments();
                int cameraMovement = (Integer) arguments.get(0);

                switch (cameraMovement) {
                    case UP:
                        commandController.cameraDecY();
                        break;
                    case DOWN:
                        commandController.cameraIncY();
                        break;
                    case PANLEFT:
                        commandController.cameraDecX();
                        break;
                    case PANRIGHT:
                        commandController.cameraIncX();
                        break;
                }

            }
        };

        OSCListener commandListener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage commandMessage) {
                // Received command in arguments e.g. end etc...
                List<Object> arguments = commandMessage.getArguments();
                int command = (Integer) arguments.get(0);

                if (command == END) {
                    keepAlive = false;
                }
            }
        };

        try {
            // Create a OSC Port for receiving messages
            OSCPortIn receiver = new OSCPortIn(OSCportIn);
            // Add a listener for message types
            receiver.addListener("/direction", directionListener);
            receiver.addListener("/camera", cameraListener);
            receiver.addListener("/command", commandListener);
            // start listening for messages
            receiver.startListening();
        } catch (SocketException e) {
            e.printStackTrace();
            System.out.println("Unable to Open OSC Port");
        }

        while (keepAlive) {
            // Ensures App keep running until it finishes with messages
        }
    }
}
