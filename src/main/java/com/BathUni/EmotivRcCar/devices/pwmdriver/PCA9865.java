package com.BathUni.EmotivRcCar.devices.pwmdriver;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import java.io.IOException;

/****
 * Port of python code to java based code
 * Sources - https://github.com/OlivierLD/raspberry-pi4j-samples/blob/master/I2C.SPI/src/i2c/servo/adafruitmotorhat/PWM.java
 * https://github.com/sunfounder/Sunfounder_Smart_Video_Car_Kit_for_RaspberryPi/blob/master/server/PCA9685.py
 */
public class PCA9865 {

    public final static int MODE1 = 0x00;
    public final static int MODE2 = 0x01;
    public final static int SUBADR1 = 0x02;
    public final static int SUBADR2 = 0x03;
    public final static int SUBADR3 = 0x04;
    public final static int PRESCALE = 0xFE;
    public final static int LED0_ON_L = 0x06;
    public final static int LED0_ON_H = 0x07;
    public final static int LED0_OFF_L = 0x08;
    public final static int LED0_OFF_H = 0x09;
    public final static int ALL_LED_ON_L = 0xFA;
    public final static int ALL_LED_ON_H = 0xFB;
    public final static int ALL_LED_OFF_L = 0xFC;
    public final static int ALL_LED_OFF_H = 0xFD;
    public final static int RESTART = 0x80;
    public final static int SLEEP = 0x10;
    public final static int ALLCALL = 0x01;
    public final static int INVRT = 0x10;
    public final static int OUTDRV = 0x04;

    private static boolean DEBUG = true;
    private I2CDevice servoDriver;
    private final static int SERVO_ADDRESS = 0x40;

    public PCA9865() {
        this(SERVO_ADDRESS);
    }

    public PCA9865(int address) {
        try {
            I2CBus bus = I2CFactory.getInstance(I2CBus.BUS_1);
            if (DEBUG) {
                System.out.println("Connected to bus. OK.");
            }

            servoDriver = bus.getDevice(address);
            if (DEBUG) {
                System.out.println("Connected to device. OK.");
            }

            setPCA9865Freq(60);
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        } catch (UnsupportedBusNumberException ubne) {
            System.err.println(ubne.getMessage());
        }

        try {
            if (DEBUG) {
                System.out.println("Reseting PCA9865 MODE1 (without SLEEP) and MODE2");
            }
            this.setAllPCA9865((byte) 0, (byte) 0);

            if (DEBUG) {
                System.out.printf("01 - Writing 0x%02x to register 0x%02x\n", OUTDRV, MODE2);
            }
            this.servoDriver.write(MODE2, (byte) OUTDRV);

            if (DEBUG) {
                System.out.printf("02 - Writing 0x%02x to register 0x%02x\n", ALLCALL, MODE1);
            }
            this.servoDriver.write(MODE1, (byte) ALLCALL);

            delay(5); // wait for oscillator

            int mode1 = this.servoDriver.read(MODE1);
            if (DEBUG)
                System.out.printf("03 - Device 0x%02x returned 0x%02x from register 0x%02x\n", address, mode1, MODE1);
            mode1 = mode1 & ~SLEEP; // wake up (reset sleep)
            if (DEBUG)
                System.out.printf("04 - Writing 0x%02x to register 0x%02x\n", mode1, MODE1);
            this.servoDriver.write(MODE1, (byte) mode1);
            delay(5); // wait for oscillator
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private static void delay(long t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException ie) {
            System.out.println(ie.getMessage());
        }
    }

    public void setAllPCA9865(byte on, byte off) throws IOException {
        // Sets a all PWM channels
        if (DEBUG) {
            System.out.printf("05 - Writing 0x%02x to register 0x%02x\n", (on & 0xFF), ALL_LED_ON_L);
            System.out.printf("06 - Writing 0x%02x to register 0x%02x\n", (on >> 8), ALL_LED_ON_H);
            System.out.printf("07 - Writing 0x%02x to register 0x%02x\n", (off & 0xFF), ALL_LED_OFF_L);
            System.out.printf("08 - Writing 0x%02x to register 0x%02x\n", (off >> 8), ALL_LED_OFF_H);
        }

        this.servoDriver.write(ALL_LED_ON_L, (byte) (on & 0xFF));
        this.servoDriver.write(ALL_LED_ON_H, (byte) (on >> 8));
        this.servoDriver.write(ALL_LED_OFF_L, (byte) (off & 0xFF));
        this.servoDriver.write(ALL_LED_OFF_H, (byte) (off >> 8));
    }

    public void setPCA9865(int channel, short on, short off) throws IOException {
        // Sets a single PWM channel
        if (DEBUG) {
            System.out.printf("ON:0x%02x, OFF:0x%02x\n", on, off);
            System.out.printf("09 - Writing 0x%02x to register 0x%02x\n", (on & 0xFF), LED0_ON_L + 4 * channel);
            System.out.printf("10 - Writing 0x%02x to register 0x%02x\n", (on >> 8) & 0xFF, LED0_ON_H + 4 * channel);
            System.out.printf("11 - Writing 0x%02x to register 0x%02x\n", (off & 0xFF), LED0_OFF_L + 4 * channel);
            System.out.printf("12 - Writing 0x%02x to register 0x%02x\n", (off >> 8) & 0xFF, LED0_OFF_H + 4 * channel);
        }

        this.servoDriver.write(LED0_ON_L + 4 * channel, (byte) (on & 0xFF));
        this.servoDriver.write(LED0_ON_H + 4 * channel, (byte) ((on >> 8) & 0xFF));
        this.servoDriver.write(LED0_OFF_L + 4 * channel, (byte) (off & 0xFF));
        this.servoDriver.write(LED0_OFF_H + 4 * channel, (byte) ((off >> 8) & 0xFF));
    }

    public void setPCA9865Freq(int freq) throws IOException {

        // Sets the PWM frequency
        double preScaleVal = 25000000.0; // 25MHz
        preScaleVal /= 4096.0; // 12-bit
        preScaleVal /= (float) freq;
        preScaleVal -= 1.0;
        if (DEBUG) {
            System.out.println("Setting PWM frequency to " + freq + " Hz");
            System.out.println("Estimated pre-scale:" + preScaleVal);
        }

        double preScale = Math.floor(preScaleVal + 0.5);
        if (DEBUG) {
            System.out.println("Final pre-scale: " + preScale);
        }

        int oldMode = this.servoDriver.read(MODE1);
        byte newMode = (byte) ((oldMode & 0x7F) | 0x10); // sleep
        if (DEBUG) {
            System.out.printf("13 - Writing 0x%02x to register 0x%02x\n", newMode, MODE1);
            System.out.printf("14 - Writing 0x%02x to register 0x%02x\n", (byte) (Math.floor(preScale)), PRESCALE);
            System.out.printf("15 - Writing 0x%02x to register 0x%02x\n", oldMode, MODE1);
        }

        this.servoDriver.write(MODE1, newMode); // go to sleep
        this.servoDriver.write(PRESCALE, (byte) (Math.floor(preScale)));
        this.servoDriver.write(MODE1, (byte) oldMode);

        delay(5);

        if (DEBUG) {
            System.out.printf("16 - Writing 0x%02x to register 0x%02x\n", (oldMode | 0x80), MODE1);
        }
        this.servoDriver.write(MODE1, (byte) (oldMode | 0x80));
    }

    private void softwareReset() {
        if (DEBUG) {
            System.out.println("Reset");
        }

        try {
            servoDriver.write((byte) 6);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
