package com.BathUni.EmotivRcCar.devices.camera;

import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;
import java.io.IOException;

public class Camera {

    public static int minPulse = 200;
    public static int maxPulse = 700;

    public static int current_x = 0;
    public static int current_y = 0;

    public static int offset_x = 0;
    public static int offset_y = 0;

    public static int Xmin, Ymin, Xmax, Ymax, home_x, home_y;

    private PCA9865 servo;

    public Camera(PCA9865 pca9865){
        this.servo = pca9865;
        Xmin = minPulse + offset_x;
        Xmax = maxPulse + offset_x;
        Ymin = minPulse + offset_y;
        Ymax = maxPulse + offset_y;
        home_x = (Xmax + Xmin)/2;
        home_y = Ymin + 80;
        homeXY();
    }


    public void moveDecreaseX(){
        current_x += 25;
        if (current_x > Xmax) {
            current_x = Xmax;
        }
        try {
            servo.setPCA9865(14,(short)0,(short)current_x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void moveIncreaseX(){
        current_x -= 25;
        if (current_x <= Xmin) {
            current_x = Xmin;
        }
        try {
            servo.setPCA9865(14,(short)0,(short)current_x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void moveDecreaseY(){
        current_y += 25;
        if (current_y > Ymax) {
            current_y = Ymax;
        }
        try {
            servo.setPCA9865(15,(short)0,(short)current_y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void moveIncreaseY(){
        current_y -= 25;
        if (current_y <= Ymin) {
            current_y = Ymin;
        }
        try {
            servo.setPCA9865(15,(short)0,(short)current_y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void homeXY(){
        current_y = home_y;
        current_x = home_x;
        try {
            servo.setPCA9865(14, (short)0, (short)current_x);
            servo.setPCA9865(15, (short)0, (short)current_y);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void calibrate(int x,int y){
        try {
            servo.setPCA9865(14, (short)0, (short)((maxPulse+minPulse)/2+x));
            servo.setPCA9865(15, (short)0, (short)((maxPulse+minPulse)/2+y));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
