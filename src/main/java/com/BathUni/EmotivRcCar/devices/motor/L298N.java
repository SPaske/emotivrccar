package com.BathUni.EmotivRcCar.devices.motor;

import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;
import com.pi4j.io.gpio.*;
import java.io.IOException;

public class L298N {

    // ===========================================================================
    // Raspberry Pi pin11, 12, 13 and 15 to realize the clockwise/counterclockwise
    // rotation and forward and backward movements
    // ===========================================================================
    private static Pin Motor0_A = RaspiPin.GPIO_00; // pin 11
    private static Pin Motor0_B = RaspiPin.GPIO_01; // pin 12
    private static Pin Motor1_A = RaspiPin.GPIO_02; // pin13
    private static Pin Motor1_B = RaspiPin.GPIO_03; // pin15

    // ===========================================================================
    // Set channel 4 and 5 of the servo driver IC to generate PWM, thus
    // controlling the speed of the car
    // ==========================================================================
    private static int EN_M0 = 4; // Servo driver IC CH4
    private static int EN_M1 = 5; // Servo driver IC CH5

    private GpioPinDigitalOutput pin0A, pin0B, pin1A, pin1B;

    private PCA9865 pcs9865;
    final GpioController gpio = GpioFactory.getInstance();

    private boolean forward0 = true, forward1 = false, backward0 = false, backward1 = true;

    public L298N(PCA9865 pca9865) {
        this.pcs9865 = pca9865;
        setup();
        setSpeed((short)100);
        stop();
    }

    // ===========================================================================
    // Adjust the duty cycle of the square waves output from channel 4 and 5 of
    // the servo driver IC, so as to control the speed of the car.
    // ===========================================================================
    public void setSpeed(short speed) {
        speed *= 40;
        try {
            pcs9865.setPCA9865(EN_M0, (short) 0, speed);
            pcs9865.setPCA9865(EN_M1, (short) 0, speed);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void stop() {
        pin0A.low();
        pin0B.low();
        pin1A.low();
        pin1B.low();
    }

    public void foward() {
        motor0(forward0);
        motor1(forward1);
    }

    public void backward() {
        motor0(backward0);
        motor1(backward1);
    }

    public void fowardWithSpeed(short speed) {
        setSpeed(speed);
        motor0(forward0);
        motor1(forward1);
    }

    public void backwardWithSpeed(short speed) {
        setSpeed(speed);
        motor0(backward0);
        motor1(backward1);
    }

    public void ctrl(boolean status, boolean direction) {
        if (status) {
            if (direction) {
                foward();
            } else {
                backward();
            }
        } else {
            stop();
        }
    }

    private void motor0(boolean state) {
        setMotorState(pin0A, pin0B, state);
    }

    private void motor1(boolean state) {
        setMotorState(pin1A, pin1B, state);
    }

    private void setMotorState(GpioPinDigitalOutput pinA, GpioPinDigitalOutput pinB, boolean state) {
        if (state) {
            pinA.low();
            pinB.high();
        } else {
            pinA.high();
            pinB.low();
        }
    }

    private void setup() {
        pin0A = gpio.provisionDigitalOutputPin(Motor0_A);
        pin0A.setShutdownOptions(true, PinState.LOW);
        pin0B = gpio.provisionDigitalOutputPin(Motor0_B);
        pin0B.setShutdownOptions(true, PinState.LOW);
        pin1A = gpio.provisionDigitalOutputPin(Motor1_A);
        pin1A.setShutdownOptions(true, PinState.LOW);
        pin1B = gpio.provisionDigitalOutputPin(Motor1_B);
        pin1B.setShutdownOptions(true, PinState.LOW);
    }


}
