package com.BathUni.EmotivRcCar.devices.steering;

import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;
import java.io.IOException;

public class Steering {

    private static int leftPWM = 380;
    private static int homePWM = 480;
    private static int rightPWM = 580;
    private static int offset =0;

    PCA9865 servo;

    public Steering(PCA9865 pca9865){
        this(pca9865, offset);
    }

    public Steering (PCA9865 pca9865, int offset){
        this.servo = pca9865;
        leftPWM += offset;
        homePWM += offset;
        rightPWM += offset;
        home();
    }

    private int map(int x, int in_min, int in_max, int out_min, int out_max){
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    public void turnLeft(){
        try {
            servo.setPCA9865(0,(short)0,(short)leftPWM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void turnRight(){
        try {
            servo.setPCA9865(0,(short)0,(short)rightPWM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void turn(int angle){
        try {
            int steerAngle = map(angle, 0, 255, leftPWM, rightPWM);
            servo.setPCA9865(0,(short)0,(short)steerAngle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void home(){
        try {
            servo.setPCA9865(0,(short)0,(short)homePWM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void calibrate(int x){
        try {
            servo.setPCA9865(0,(short)0,(short)(450+x));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
