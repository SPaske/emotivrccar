package com.BathUni.EmotivRcCar.configuration;

import com.BathUni.EmotivRcCar.Services.*;
import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public PCA9865 pca9865(){
        return new PCA9865();
    }

    @Bean
    public ICameraService cameraService(PCA9865 pca9865){
        return new CameraService(pca9865);
    }

    @Bean
    public IMotorService motorService(PCA9865 pca9865){
        return new MotorService(pca9865);
    }

    @Bean
    public ISteeringService steeringService(PCA9865 pca9865){
        return new SteeringService(pca9865);
    }

}
