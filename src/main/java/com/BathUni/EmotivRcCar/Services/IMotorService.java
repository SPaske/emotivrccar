package com.BathUni.EmotivRcCar.Services;

public interface IMotorService {

    void setSpeed(short speed);

    void stop();

    void foward();

    void backward();

    void fowardWithSpeed(short speed);

    void backwardWithSpeed(short speed);

    void ctrl(boolean status, boolean direction);
}
