package com.BathUni.EmotivRcCar.Services;

public interface ICameraService {

    void moveDecreaseX();

    void moveIncreaseX();

    void moveDecreaseY();

    void moveIncreaseY();

    void homeXY();

    void calibrate(int x,int y);
}
