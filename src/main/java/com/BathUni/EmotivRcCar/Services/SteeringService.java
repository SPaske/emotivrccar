package com.BathUni.EmotivRcCar.Services;

import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;
import com.BathUni.EmotivRcCar.devices.steering.Steering;

public class SteeringService implements  ISteeringService{

    Steering steering;

    public SteeringService(PCA9865 pca9865) {
        this.steering = new Steering(pca9865);
    }

    public void turnLeft() {
        steering.turnLeft();
    }

    public void turnRight() {
        steering.turnRight();
    }

    public void turn(int angle) {
        steering.turn(angle);
    }

    public void home() {
        steering.home();
    }

    public void calibrate(int x) {
        steering.calibrate(x);
    }
}
