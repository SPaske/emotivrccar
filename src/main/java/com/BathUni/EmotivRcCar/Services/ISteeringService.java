package com.BathUni.EmotivRcCar.Services;

public interface ISteeringService {

    void turnLeft();

    void turnRight();

    void turn(int angle);

    void home();

    void calibrate(int x);
}
