package com.BathUni.EmotivRcCar.Services;

import com.BathUni.EmotivRcCar.devices.motor.L298N;
import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;

public class MotorService implements  IMotorService{

    L298N motors;

    public MotorService(PCA9865 pca9865) {
        this.motors = new L298N(pca9865);
    }

    public void foward() {
        motors.foward();
    }

    public void backward() {
        motors.backward();
    }

    public void fowardWithSpeed(short speed) {
        motors.fowardWithSpeed(speed);
    }

    public void backwardWithSpeed(short speed) {
        motors.backwardWithSpeed(speed);
    }

    public void ctrl(boolean status, boolean direction) {
        motors.ctrl(status, direction);
    }

    public void setSpeed(short speed) {
        motors.setSpeed(speed);
    }

    public void stop() {
        motors.stop();
    }

}
