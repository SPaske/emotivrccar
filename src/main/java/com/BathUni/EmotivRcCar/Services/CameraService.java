package com.BathUni.EmotivRcCar.Services;

import com.BathUni.EmotivRcCar.devices.camera.Camera;
import com.BathUni.EmotivRcCar.devices.pwmdriver.PCA9865;

public class CameraService implements  ICameraService{

    Camera camera;

    public CameraService(PCA9865 pca9865) {
        this.camera = new Camera(pca9865);
    }

    public void moveDecreaseX() {
        camera.moveDecreaseX();
    }

    public void moveIncreaseX() {
        camera.moveIncreaseX();
    }

    public void moveDecreaseY() {
        camera.moveDecreaseY();
    }

    public void moveIncreaseY() {camera.moveIncreaseY();}

    public void homeXY() {
        camera.homeXY();
    }

    public void calibrate(int x, int y) {
        camera.calibrate(x, y);
    }
}
